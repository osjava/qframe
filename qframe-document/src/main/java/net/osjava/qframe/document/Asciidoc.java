package net.osjava.qframe.document;

import com.sun.javadoc.DocErrorReporter;
import com.sun.javadoc.Doclet;
import com.sun.javadoc.LanguageVersion;
import com.sun.javadoc.RootDoc;

/**
 * A link:Doclet[] that exports javadoc comments containing AsciiDoc text to AsciiDoc files,
 * instead of specific final formats such as HTML.
 *
 * @author John Ericksen
 * @see AsciidocRenderer
 */
public class Asciidoc extends Doclet
{
    private final AsciidocRenderer renderer;

    /**
     * Starts the doclet.
     * @param rootDoc the root of the program structure information.
     *                From this root all other program structure information can be extracted.
     * @return true if the doclet was started successfuly, false otherwise
     * @see Doclet#start(RootDoc)
     */
    @SuppressWarnings("UnusedDeclaration")
    public static boolean start(RootDoc rootDoc) {
        return new Asciidoc(rootDoc).render();
    }

    /**
     * Creates a ExportDoclet to export javadoc comments to AsciiDoc files.
     *
     * @param rootDoc the root of the program structure information.
     *                From this root all other program structure information can be extracted.
     */
    public Asciidoc(RootDoc rootDoc) {
        this.renderer = new AsciidocRenderer(rootDoc);
    }

    /**
     * Validates command line options.
     *
     * @param options the array of given options
     * @param errorReporter an object that allows printing error messages for invalid options
     * @return true if the options are valid, false otherwise
     * @see Doclet#validOptions(String[][], DocErrorReporter)
     */
    @SuppressWarnings("UnusedDeclaration")
    public static boolean validOptions(String[][] options, DocErrorReporter errorReporter) {
        return new StandardAdapter().validOptions(options, errorReporter);
    }

    /**
     * Gets the number of arguments that a given command line option must contain.
     * @param option the command line option
     * @return the number of arguments required for the given option
     * @see Doclet#optionLength(String)
     */
    @SuppressWarnings("UnusedDeclaration")
    public static int optionLength(String option) {
        return new StandardAdapter().optionLength(option);
    }

    /**
     * Return the version of the Java Programming Language supported
     * by this doclet.
     * @return the Java language supported version
     * @see Doclet#languageVersion()
     */
    @SuppressWarnings("UnusedDeclaration")
    public static LanguageVersion languageVersion() {
        return LanguageVersion.JAVA_1_5;
    }

    /**
     * Renders the javadoc documentation for all elements inside the link:RootDoc[] object
     * received by this doclet.
     * @return true if the link:RootDoc[] was rendered successfully, false otherwise
     */
    private boolean render(){
        return renderer.render();
    }
}
