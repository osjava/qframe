package net.osjava.qframe.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.osjava.qframe.database.IEntity;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity(name = "SYS_USER")
@Data
@EqualsAndHashCode(callSuper = false)
@RequiredArgsConstructor(staticName = "with")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SysUser implements IEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "UID", nullable = false)
    private Long uid = 0L;

    @Column(name = "USER_NAME", nullable = false)
    @NonNull private String userName;

    @Column(name = "USER_PWD", nullable = false)
    @NonNull private String userPwd;

    @Column(name = "DISABLED", nullable = false)
    private boolean disabled = false;

    @Column(name = "CREATE_TIME", nullable = false)
    private LocalDateTime createTime = LocalDateTime.now();

    public boolean validPassword(String password) {
        String encode = DigestUtils.md5Hex(password);
        return StringUtils.equalsAnyIgnoreCase(this.userPwd, encode);
    }
}
