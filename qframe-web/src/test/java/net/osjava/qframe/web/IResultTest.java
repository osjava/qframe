package net.osjava.qframe.web;

import org.junit.jupiter.api.Test;

import java.util.function.Function;

class IResultTest
{

    private void render(Function<RequestContext, IResult<ViewPath>> method) {
        System.out.println(method.getClass().getCanonicalName());
        System.out.println(method.getClass().getSuperclass());

    }

    @Test void test() {
        TestClass test = new TestClass();
        render(test::index);
    }
}

class TestClass
{
    public IResult<ViewPath> index(RequestContext rc) {
        return IResult.nothing();
    }
}