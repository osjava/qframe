package net.osjava.qframe.core;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

class EventServiceTest
{
    @Test void Completable_future() {
        CompletableFuture.runAsync(() -> {
            sleep(2000);
            System.out.println("Running 1 - " + System.currentTimeMillis());
        }).whenComplete((v, e)->System.out.println("Finished 1"));

        CompletableFuture.runAsync(() -> {
            sleep(1000);
            System.out.println("Running 2 - " + System.currentTimeMillis());
        });

        sleep(4000);
    }

    @SneakyThrows private void sleep(int timeout) {
        TimeUnit.MILLISECONDS.sleep(timeout);
    }
}