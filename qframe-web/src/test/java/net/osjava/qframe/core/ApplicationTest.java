package net.osjava.qframe.core;

import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.database.Repository;
import net.osjava.qframe.domain.SysUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class ApplicationTest extends AbstractApplication
{
    @BeforeAll void beforeAll() {
        log.debug("Start Unit Test...");
        Properties appProperties = new Properties();
        appProperties.setProperty("A1", "V1");

        super.init("unit_test", "src/test/workspace", appProperties);
        log.debug("Finish Unit Test.");
    }

    @Test void init_application() {
        log.debug("Init application...");
        JdbcTemplate jdbcTemplate = BeanFactory.getBean(JdbcTemplate.class);
        List<Map<String, Object>> result = jdbcTemplate.queryForList("SELECT * FROM SYS_USER WHERE USER_NAME=?", "admin");
        Assertions.assertFalse(result.isEmpty());

        Repository repository = BeanFactory.getBean(Repository.class);

        Optional<SysUser> entity = repository.findFirstBy(SysUser.class, "userName", "admin");
        Assertions.assertTrue(entity.isPresent());
    }
}