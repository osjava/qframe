package net.osjava.qframe.utils;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PathUtilsTest
{
    @Test void Zip_file()
    throws IOException {
        String source = new File("src/test/workspace/conf/log4j-config.xml").getAbsolutePath();
        String target = new File("src/test/workspace/conf/log4j-config.zip").getAbsolutePath();

        PathUtils.zip(target, source);

        assertTrue(PathUtils.isFile(target));
        PathUtils.remove(target);
    }

    @Test void Zip_director()
    throws IOException {
        String source = new File("src/test/workspace").getAbsolutePath();
        String target = new File("src/test/workspace.zip").getAbsolutePath();

        PathUtils.zip(target, source);
        assertTrue(PathUtils.isFile(target));
        PathUtils.remove(target);
    }

    @Test void Unzip_file()
    throws IOException {
        String source = new File("src/test/workspace/conf/log4j-config.xml").getAbsolutePath();
        String zip = new File("src/test/workspace/conf/log4j-config.zip").getAbsolutePath();

        PathUtils.zip(zip, source);

        String unzip = new File("src/test/workspace/log4j-config.xml").getAbsolutePath();
        PathUtils.unzip(zip, unzip);
        assertTrue(PathUtils.isFile(unzip));
    }

    @Test void Unzip_directory()
    throws IOException {
        String source = new File("src/test/workspace").getAbsolutePath();
        String target = new File("src/test/workspace.zip").getAbsolutePath();

        PathUtils.zip(target, source);

        PathUtils.unzip(target, source);

        assertTrue(PathUtils.isDirectory(PathUtils.concat(source, "workspace")));
        PathUtils.remove(PathUtils.concat(source, "workspace"));
        PathUtils.remove(target);
    }

    @Test void Move_file()
    throws IOException {
        String source = new File("src/test/workspace/conf/log4j-config.xml").getAbsolutePath();
        String zip = new File("src/test/workspace/conf/log4j-config.zip").getAbsolutePath();

        PathUtils.zip(zip, source);

        String target = new File("src/test/workspace/log4j-config.new.zip").getAbsolutePath();
        PathUtils.move(zip, target);
        assertTrue(PathUtils.isFile(target));
        PathUtils.remove(target);
    }
}