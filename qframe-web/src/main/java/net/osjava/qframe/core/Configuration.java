package net.osjava.qframe.core;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.exception.FileNotFoundException;
import net.osjava.qframe.utils.PathUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Configuration
{
    private static final Properties properties = new Properties();
    @Getter private static String appId;
    @Getter private static String workspace;

    static void init(@NotNull String id, @NotNull String homePath, Properties mergeProperties) {
        appId = id;
        workspace = homePath;
        File confFile = new File(PathUtils.concat(homePath, "/conf/config.properties"));
        if (!confFile.exists() || confFile.isDirectory()) {
            String msg = String.format("Not found the configuration of %s", confFile.getAbsolutePath());
            log.error(msg);
            throw FileNotFoundException.bind(msg).get();
        }

        try {
            properties.load(new FileInputStream(confFile));
        } catch (IOException e) {
            // ignore this net.osjava.qframe.exception because the file had been checked above
        }

        if (mergeProperties != null) {
            properties.putAll(mergeProperties);
        }

        if (log.isDebugEnabled()) {
            StringBuilder builder = new StringBuilder(1024);
            builder.append("Init application configuration by the file: ")
                   .append(confFile.getName());

            properties.forEach((key, value) -> builder.append("\n")
                                                      .append(key)
                                                      .append("=>")
                                                      .append(value));

            log.debug(builder.toString());
        }
    }

    static Properties properties() {
        return properties;
    }

    public static Optional<String> get(@NotNull String key) {
        return Optional.ofNullable(properties.getProperty(key)).filter(StringUtils::isNotEmpty);
    }

    public static int get(@NotNull String key, int def) {
        return Integer.parseInt(get(key).orElse(String.valueOf(def)));
    }

    public static long get(@NotNull String key, long def) {
        return Long.parseLong(get(key).orElse(String.valueOf(def)));
    }

    public static boolean get(@NotNull String key, boolean def) {
        return Boolean.parseBoolean(get(key).orElse(String.valueOf(def)));
    }
}
