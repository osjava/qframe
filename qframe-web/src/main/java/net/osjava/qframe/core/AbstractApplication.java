package net.osjava.qframe.core;

import lombok.extern.java.Log;
import net.osjava.qframe.datatype.CoreEvents;
import net.osjava.qframe.utils.HttpUtils;
import net.osjava.qframe.utils.PathUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.jetbrains.annotations.NotNull;

import java.util.Properties;

@Log
public abstract class AbstractApplication
{
    protected void init(String appId, String workspace) {
        init(appId, workspace, null);
    }
    protected void init(@NotNull String appId, @NotNull String workspace, Properties properties) {
        log.info(String.format("Init Application with workspace: %s", workspace));

        String cfgLog = PathUtils.concat(workspace, "/conf/log4j-config.xml");

        log.info(String.format("Init Logger by %s", cfgLog));
        Configurator.initialize(appId, cfgLog);

        Configuration.init(appId, workspace, properties);

        BeanFactory.init();

        EventService eventService = BeanFactory.getBean(EventService.class);
        eventService.executeEvent(new CoreEvents.InitApplication(this, workspace));

        HttpUtils.init();
        log.info("Init Application successful.");
    }

    protected void destroy() {
        HttpUtils.destroy();
        EventService eventService = BeanFactory.getBean(EventService.class);
        eventService.executeEvent(new CoreEvents.DestroyApplication(this, Configuration.getWorkspace()));
        eventService.shutdown();
        LogManager.shutdown();
    }
}
