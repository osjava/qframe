package net.osjava.qframe.core;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Optional;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BeanFactory
{
    private static final String SPRING_CONTEXT = "classpath*:/META-INF/spring-beans.xml";
    private static ApplicationContext applicationContext = null;

    static void init() {
        applicationContext = new ClassPathXmlApplicationContext(SPRING_CONTEXT);
    }

    public static <T> T getBean(@NotNull Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    public static <T> Optional<T> getBean(@NotNull String name, @NotNull Class<T> clazz) {
        try {
            return Optional.of(getApplicationContext().getBean(name, clazz));
        } catch (Exception e) {
            log.error("Get the bean [{}] from application context has error: {}", name, e.getMessage());
            return Optional.empty();
        }
    }

    public static Map<String, Object> getBeansWithAnnotation(@NotNull Class<? extends Annotation> clazz) {
        return getApplicationContext().getBeansWithAnnotation(clazz);
    }

    public static boolean containsBean(@NotNull String name) {
        return getApplicationContext().containsBean(name);
    }

    private static ApplicationContext getApplicationContext() {
        return applicationContext == null ? new ClassPathXmlApplicationContext(SPRING_CONTEXT) : applicationContext;
    }
}
