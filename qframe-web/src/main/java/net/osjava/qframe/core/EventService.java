package net.osjava.qframe.core;

import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.datatype.Event;
import net.osjava.qframe.datatype.EventListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
@SuppressWarnings({"rawtypes", "unchecked"})
public class EventService
{
    private final Map<Class<? extends Event>, List<EventListener>> listenerMap = new HashMap<>();
    private final ExecutorService threadPool = Executors.newFixedThreadPool(200);

    synchronized void shutdown() {
        threadPool.shutdown();
    }

    public synchronized void registerListener(@NotNull Class<? extends Event> eventClass, @NotNull EventListener listener) {
        log.info("Add a listener {} to {}", listener, eventClass.getName());
        List<EventListener> listeners = listenerMap.computeIfAbsent(eventClass, k -> new ArrayList<>());
        listeners.add(listener);
    }

    public synchronized void executeEvent(@NotNull Event event) {
        List<EventListener> listeners = listenerMap.get(event.getClass());
        if (listeners == null) {
            log.info("Not found any listener of the event: {}", event.getClass().getName());
            return;
        }

        for (EventListener item : listeners) {
            try {
                item.onEvent(event);
            } catch (Exception e) {
                log.error("Event listener [{}] net.osjava.qframe.exception: {}", item.getClass().getName(),
                          e.getMessage());
            }
        }
    }

    public synchronized void dispatchEvent(@NotNull Event event) {
        List<EventListener> listeners = listenerMap.get(event.getClass());

        CompletableFuture.runAsync(() -> {
            for (EventListener item : listeners) {
                log.debug("Running event listener: {} with event={}", item.getClass().getName(),
                          event.getClass().getName());
                item.onEvent(event);
            }
        }, threadPool);
    }
}
