package net.osjava.qframe.core;

public interface RefSelf
{
    @SuppressWarnings("unchecked")
    default  <T extends RefSelf> T self() {
        return (T) RefSelf.this;
    }
}
