package net.osjava.qframe.web;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.osjava.qframe.core.RefSelf;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor(staticName = "of")
public class ViewPath implements RefSelf
{
    @Getter private final Map<String, Object> params = new HashMap<>();
    @Getter private final String url;
    @Getter private boolean redirect = false;

    public static ViewPath redirect(String url) {
        ViewPath viewPath = ViewPath.of(url);
        viewPath.redirect = true;
        return viewPath;
    }

    public ViewPath with(String name, Object value) {
        if (value instanceof Optional) {
            Optional<?> optValue = (Optional<?>) value;
            optValue.ifPresent(o -> this.params.put(name, o));
        } else if (value != null) {
            this.params.put(name, value);
        }

        return this.self();
    }

    @Override public String toString() {
        if (params.isEmpty()) {
            return String.format("ViewPath: %s", this.url);
        } else {
            return String.format("ViewPath: %s->%s", this.url, this.params);
        }
    }
}
