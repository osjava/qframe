package net.osjava.qframe.web;

import com.google.gson.JsonElement;
import net.osjava.qframe.utils.PathUtils;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface IResult<T>
{
    static <V> IResult<V> nothing() {
        return new IResult<V>()
        {
            @Override public void execute(RequestContext rc)
            throws IOException { }

            @Override public Optional<V> getData() {
                return Optional.empty();
            }
        };
    }

    static IResult<String> render(String value) {
        return new IResult<String>()
        {
            @Override public void execute(RequestContext rc)
            throws IOException {
                try (PrintWriter writer = rc.getResponse().getWriter()) {
                    writer.println(value);
                }
            }

            @Override public Optional<String> getData() {
                return Optional.ofNullable(value);
            }
        };
    }

    static IResult<JsonElement> render(JsonElement jsonElement) {
        return new IResult<JsonElement>()
        {
            @Override public void execute(RequestContext rc)
            throws IOException {
                rc.closeCache();

                rc.getResponse()
                  .setContentType("application/json;charset=UTF-8");

                try (PrintWriter writer = rc.getResponse()
                                            .getWriter()) {
                    writer.println(jsonElement.toString());
                }
            }

            @Override public Optional<JsonElement> getData() {
                return Optional.ofNullable(jsonElement);
            }
        };
    }

    static IResult<ViewPath> render(ViewPath viewPath) {
        return new IResult<ViewPath>()
        {
            private final URLCodec urlCodec = new URLCodec("UTF-8");
            
            @Override public void execute(RequestContext rc)
            throws IOException {
                List<String> params = new ArrayList<>();
                if (StringUtils.startsWith(viewPath.getUrl(), "http")) {
                    viewPath.getParams()
                            .forEach((k, v) -> params.add(String.format("%s=%s", k, v)));

                    String url = CollectionUtils.isEmpty(
                        params) ? viewPath.getUrl() : viewPath.getUrl() + "?" + StringUtils.join(params, "&");
                    rc.redirect(url);
                } else if (viewPath.isRedirect()) {
                    String query = viewPath.getParams().entrySet().stream()
                                           .map(it -> {
                                               String value;
                                               try {
                                                   value = urlCodec.encode(it.getValue().toString());
                                               } catch (EncoderException e) {
                                                   value = it.getValue().toString();
                                               }
                                               return String.format("%s=%s", it.getKey(), value);
                                           })
                                           .collect(Collectors.joining("&"));

                    String url = StringUtils.isEmpty(query) ? viewPath.getUrl() : viewPath.getUrl() + "?" + query;
                    rc.redirect(PathUtils.concat(rc.getContext().getContextPath(), url));
                } else {
                    String viewServlet = rc.get(RequestContext.PARAM_VIEW_SERVLET, new RuntimeException());
                    viewPath.getParams()
                            .forEach(rc::setAttribute);
                    rc.forward(PathUtils.concat(viewServlet, viewPath.getUrl()));
                }
            }

            @Override public Optional<ViewPath> getData() {
                return Optional.ofNullable(viewPath);
            }
        };
    }

    void execute(RequestContext rc)
    throws IOException;

    Optional<T> getData();
}
