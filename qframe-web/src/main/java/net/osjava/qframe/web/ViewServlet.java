package net.osjava.qframe.web;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.loader.ServletLoader;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.utils.PathUtils;
import net.osjava.qframe.web.pebble.WebExtension;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.equalsAnyIgnoreCase;

@Slf4j
public class ViewServlet extends HttpServlet
{
    private static final String defaultTemplate = "/index";
    protected String templateExt = ".html.twig";
    protected String templatePrefix = "/WEB-INF/templates";
    protected String templateTheme = "/default";

    private List<String> caches = new ArrayList<>(128);

    private PebbleEngine pebbleEngine;

    @Override public void init(ServletConfig config)
    throws ServletException {
        super.init(config);

        this.templateExt = StringUtils.defaultIfEmpty(config.getInitParameter("ext"), templateExt);
        this.templatePrefix = StringUtils.defaultIfEmpty(config.getInitParameter("prefix-template"), templatePrefix);
        this.templateTheme = StringUtils.defaultIfEmpty(config.getInitParameter("default"), templateTheme);
        if (!StringUtils.startsWith(this.templateExt, ".")) {
            this.templateExt = "." + this.templateExt;
        }

        boolean enableCache = BooleanUtils.toBoolean(System.getProperty("ENABLE_TEMPLATE_CACHED", "true"));
        PebbleEngine.Builder builder = new PebbleEngine.Builder();
        builder = builder.cacheActive(enableCache);

        ServletLoader loader = new ServletLoader(config.getServletContext());
        loader.setPrefix(PathUtils.concat(this.templatePrefix, this.templateTheme));
//        loader.setSuffix(this.templateExt);
        builder.loader(loader);
        builder.extension(new WebExtension());

        pebbleEngine = builder.build();

        log.info("Init the ViewServlet by: template.prefix={}, template.theme={}, template.ext={}, template.cache={}",
                 this.templatePrefix, this.templateTheme, templateExt, enableCache);
    }

    @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
        this.doService(req, resp);
    }

    @Override protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
        this.doService(req, resp);
    }

    private void doService(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
        RequestContext rc = RequestContext.get();
        String uri = StringUtils.substringAfter(req.getRequestURI(), req.getServletPath());
        log.debug("Running ViewServlet.doService(): uri={}", uri);

        if (StringUtils.endsWith(uri, this.templateExt)) {
            uri = StringUtils.substringBeforeLast(uri, this.templateExt);
        }

        String tpl = getTemplate(uri);
        if (StringUtils.isBlank(tpl)) {
            log.debug("Not found template for domain. search by default paths.");
            rc.errorNotFound();
            return;
        }

        String[] params = StringUtils.split(StringUtils.substringAfter(uri, tpl), "/");
        for (int i = 1, n = params.length; i <= n; i++) {
            rc.set("P" + i, params[i - 1]);
        }

        this.render(tpl + this.templateExt);
    }

    protected void render(String uri)
    throws ServletException, IOException {
        RequestContext rc = RequestContext.get();
        String templateName = uri;
        if (StringUtils.startsWith(uri, "/")) {
            templateName = StringUtils.substringAfter(uri, "/");
        }
        PebbleTemplate template = pebbleEngine.getTemplate(templateName);

        Map<String, Object> context = new HashMap<>();
        Enumeration<String> sessionNames = rc.getRequest().getSession().getAttributeNames();
        while (sessionNames.hasMoreElements()) {
            String name = sessionNames.nextElement();
            rc.getSession(name).ifPresent(it -> context.put(name, it));
        }

        Enumeration<String> names = rc.getRequest()
                                      .getAttributeNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            rc.getAttribute(name).ifPresent(it -> context.put(name, it));
        }
        log.debug("Render a view with: {}", context);
        template.evaluate(rc.getResponse().getWriter(), context);
    }

    private String getTemplate(String path) {
        // /WEB-INF/templates/default/user/member/login/index.jsp
        // /WEB-INF/templates/default/user/member/login.jsp
        // /WEB-INF/templates/default/user/member/index.jsp
        // /WEB-INF/templates/default/user/member.jsp
        // /WEB-INF/templates/default/user/index.jsp
        // /WEB-INF/templates/default/user.jsp
        // /WEB-INF/templates/default/index.jsp

        if (StringUtils.isEmpty(path)) {
            return StringUtils.EMPTY;
        }

        // Check if the default template is existing, for example: [/user/member/login/index.jsp]
        if (exist(PathUtils.concat(path, defaultTemplate))) {
            return PathUtils.concat(path, defaultTemplate);
        }

        // Check if the template is existing, for example: [/user/member/login.jsp]
        if (exist(path)) {
            return path;
        }

        // If the template is not existing, find it from its parent directory.
        return getTemplate(StringUtils.substringBeforeLast(path, "/"));
    }

    private boolean exist(String path) {
        log.debug("Check a template path is exists: {}", path);
        // Checks the path is in caches.
        if (this.caches.contains(path)) {
            return true;
        }
        if (StringUtils.isEmpty(path)) {
            return false;
        }

        RequestContext rc = RequestContext.get();

        // Checks the real file exist and it is a file.
        String realPath = rc.getContext()
                            .getRealPath(
                                PathUtils.concat(this.templatePrefix, this.templateTheme, path) + this.templateExt);
        log.debug("The real path of the template is: {}", realPath);
        File file = new File(realPath);

        if (file.exists() && file.isFile()) {
            this.caches.add(path);
            return true;
        }

        return false;
    }
}
