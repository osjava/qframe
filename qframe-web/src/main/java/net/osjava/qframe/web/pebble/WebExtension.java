package net.osjava.qframe.web.pebble;

import com.mitchellbosecke.pebble.extension.AbstractExtension;
import com.mitchellbosecke.pebble.extension.Filter;
import com.mitchellbosecke.pebble.extension.Function;

import java.util.HashMap;
import java.util.Map;

public class WebExtension extends AbstractExtension
{
    @Override public Map<String, Function> getFunctions() {
        Map<String, Function> functionMap = new HashMap<>();
        functionMap.put(PathFunction.FUNCTION_NAME, new PathFunction());
        functionMap.put(StartWithFunction.FUNCTION_NAME, new StartWithFunction());
        functionMap.put(EnvFunction.FUNCTION_NAME, new EnvFunction());

        return functionMap;
    }

    @Override public Map<String, Filter> getFilters() {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put(NumberMaskFilter.FUNCTION_NAME, new NumberMaskFilter());

        return filterMap;
    }
}
