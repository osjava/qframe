package net.osjava.qframe.web;

import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.core.BeanFactory;
import net.osjava.qframe.utils.ClassUtils;
import net.osjava.qframe.utils.PathUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static net.osjava.qframe.utils.ClassUtils.getMethod;
import static net.osjava.qframe.web.RequestContext.*;

@Slf4j
@MultipartConfig(maxFileSize = 52428800, maxRequestSize = 52428800)
public class ActionServlet extends HttpServlet
{
    private static final ServletException NO_VIEW_SERVLET_EXCEPTION = new ServletException("The View Servlet doesn't exist");
    private static final String DEFAULT_METHOD = "index";

    @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException {
        doService(req);
    }

    @Override protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException {
        doService(req);
    }

    private void doService(HttpServletRequest req)
    throws ServletException {
        RequestContext rc = RequestContext.get();

        String actionName = rc.get(PARAM_ACTION_NAME, new ServletException("The Action Name is empty."));

        String requestUri = StringUtils.substringAfter(req.getRequestURI(), req.getServletPath());
        log.debug("Execute the Action: name={}; uri={}", actionName, requestUri);

        String[] params = StringUtils.split(StringUtils.substringAfter(requestUri, actionName), "/");
        log.debug("Split the url to params: {}", StringUtils.join(params, ","));

        Optional<AbstractAction> optAction = BeanFactory.getBean(actionName, AbstractAction.class);
        if (!optAction.isPresent()) {
            log.debug("Not found the defined Bean of the action [{}]", actionName);
            String viewUri = rc.get(PARAM_VIEW_SERVLET, NO_VIEW_SERVLET_EXCEPTION);
            rc.forward(PathUtils.concat(viewUri, requestUri));
            return;
        }

        log.debug("Lookup the executable method in the action class: {}", optAction.get().getClass().getName());

        rc.set(PARAM_ACTION_METHOD, DEFAULT_METHOD);
        if (ArrayUtils.isNotEmpty(params) && getMethod(optAction.get().getClass(), params[0], RequestContext.class).isPresent()) {
            log.debug("Found the method [{}] in the action [{}]", params[0], actionName);
            for (int index = 0, count = params.length; index < count; index++) {
                rc.set(String.format("P%d", index), params[index]);
            }
        } else if(getMethod(optAction.get().getClass(), DEFAULT_METHOD, RequestContext.class).isPresent()) {
            log.debug("Found the default method [{}] in the action [{}]", DEFAULT_METHOD, actionName);
            for (int index = 1, count = params.length; index <= count; index++) {
                rc.set(String.format("P%d", index), params[index-1]);
            }
        } else {
            log.debug("Not found any executable method in the action [{}]", actionName);
            rc.forward(PathUtils.concat(rc.get(PARAM_VIEW_SERVLET, NO_VIEW_SERVLET_EXCEPTION), requestUri));
            return;
        }

        log.info(">>> Request the method [{}] of the action [{}] with {}",
                 rc.get(PARAM_ACTION_METHOD), req.getRequestURI(), rc);

        try {

            IResult<?> result = optAction.get().execute(rc);
            log.info("<<< Response from {}: {}", req.getRequestURI(), result.getData());
            result.execute(rc);
        } catch (Throwable e) {
            throw new ServletException(e);
        }
    }
}
