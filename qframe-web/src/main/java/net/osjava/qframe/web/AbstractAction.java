package net.osjava.qframe.web;

import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.core.BeanFactory;
import net.osjava.qframe.utils.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

@Slf4j
public abstract class AbstractAction
{
    protected IResult<?> execute(@NotNull RequestContext rc)
    throws Throwable {
        String methodName = rc.get(RequestContext.PARAM_ACTION_METHOD, new RuntimeException());

        Optional<Method> method = ClassUtils.getMethod(getClass(), methodName, RequestContext.class);
        if (!method.isPresent()) {
            rc.errorNotFound();
            return IResult.nothing();
        }

        ApiSecurity apiSecurity = Optional.ofNullable(method.get().getAnnotation(ApiSecurity.class))
                                          .orElse(getClass().getAnnotation(ApiSecurity.class));

        if (apiSecurity != null) {
            Optional<IApiSecurity> securityBean = BeanFactory.getBean(apiSecurity.bean(), IApiSecurity.class);

            if (!securityBean.isPresent()) {
                log.error("Not found the api security bean by name of {}", apiSecurity.bean());
                rc.errorForbidden();
                return IResult.nothing();
            }

            String signValue = Optional.ofNullable(rc.get(apiSecurity.signName()))
                                       .orElse(StringUtils.EMPTY);

            IResult<?> result = securityBean.get().validate(signValue, rc, apiSecurity);

            if (result.getData().isPresent()) return result;
        }

        try {
            return (IResult<?>) method.get().invoke(this, rc);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw e.getCause();
        }
    }
}
