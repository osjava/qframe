package net.osjava.qframe.web;

public interface IApiSecurity
{
    default IResult<?> validate(String sign, RequestContext rc, ApiSecurity config) {
        return IResult.nothing();
    }
}
