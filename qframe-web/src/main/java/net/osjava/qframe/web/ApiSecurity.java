package net.osjava.qframe.web;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiSecurity
{
    /**
     * Bean string.
     *
     * @return the string
     */
    String bean() default "apiSecurity";

    /**
     * App key name string.
     *
     * @return the string
     */
    String appKeyName() default "appKey";

    /**
     * App key value string.
     *
     * @return the string
     */
    String appKeyValue() default "";

    /**
     * Sign name string.
     *
     * @return the string
     */
    String signName() default "sign";

    /**
     * Names string [ ].
     *
     * @return the string [ ]
     */
    String[] names() default {};
}
