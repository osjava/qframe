package net.osjava.qframe.web.pebble;

import com.mitchellbosecke.pebble.extension.Function;
import com.mitchellbosecke.pebble.template.EvaluationContext;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import net.osjava.qframe.utils.PathUtils;
import net.osjava.qframe.web.RequestContext;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PathFunction implements Function
{
    static final String FUNCTION_NAME = "path";
    private static final String ARG_URL = "url";
    private static final String ARG_PARAMS = "params";

    @Override
    @SuppressWarnings("unchecked")
    public Object execute(Map<String, Object> map, PebbleTemplate pebbleTemplate, EvaluationContext evaluationContext,
                          int i) {
        String url = (String) map.get(ARG_URL);
        Map<String, Object> params = (Map<String, Object>) map.get(ARG_PARAMS);

        if (params != null) {
            List<String> urlParams = new ArrayList<>();
            params.forEach((key, value) -> urlParams.add(key + "=" + value));
            url = url + "?" + StringUtils.join(urlParams, "&");
        }

        return PathUtils.concat(RequestContext.get().getContextPath(), url);
    }

    @Override public List<String> getArgumentNames() {
        return Arrays.asList(ARG_URL, ARG_PARAMS);
    }
}
