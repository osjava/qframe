package net.osjava.qframe.web;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.core.Configuration;
import net.osjava.qframe.utils.JsonUtils;
import net.osjava.qframe.utils.PathUtils;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE, staticName = "with")
public final class RequestContext
{
    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final ThreadLocal<RequestContext> cache = new ThreadLocal<>();

    static final String PARAM_ACTION_SERVLET = "_action_servlet";
    static final String PARAM_VIEW_SERVLET = "_view_servlet";
    static final String PARAM_ACTION_METHOD = "P0";
    static final String PARAM_ACTION_NAME = "_action_name";

    @NonNull @Getter private HttpServletRequest request;
    @NonNull @Getter private HttpServletResponse response;
    @NonNull @Getter private ServletContext context;

    private final Map<String, String> params = new HashMap<>(16);
    private final Map<String, String> fileParams = new HashMap<>();

    public static RequestContext get() {return cache.get();}

    @SneakyThrows(UnsupportedEncodingException.class)
    static RequestContext create(HttpServletRequest req, HttpServletResponse resp, ServletContext context)
    throws ServletException {
        RequestContext rc = get();
        if (rc == null) {
            log.debug("Create a new instance of RequestContext");
            rc = RequestContext.with(req, resp, context);
        } else {
            log.debug("Refresh the instance of RequestContext");
            rc.request = req;
            rc.response = resp;
            rc.context = context;
        }

        rc.request.setCharacterEncoding(DEFAULT_CHARSET);

        rc.response = resp;
        rc.response.setCharacterEncoding(DEFAULT_CHARSET);
        rc.response.setHeader("P3P",
                              "CP='CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR'");
        if (Configuration.get("cross.access.enable", Boolean.FALSE)) {
            rc.response.setHeader("Access-Control-Allow-Origin", "*");
            rc.response.setHeader("Access-Control-Allow-Headers",
                                  "access-control-allow-origin,X-Requested-With,x-qframe-certificate,Content-Type");
            rc.response.setHeader("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
        }

        if (ServletFileUpload.isMultipartContent(req)) {
            rc.fileParams.putAll(parseMultipartRequest(req));
            rc.params.putAll(rc.fileParams);
        }

        // The parseMultipartRequest() only parse the parameters from the post of the Request
        rc.params.putAll(parseRequest(req));

        cache.set(rc);
        return rc;
    }

    static void destroy() {
        RequestContext rc = get();
        if (rc == null) return;

        rc.fileParams.values()
                     .forEach(PathUtils::remove);

        rc.params.clear();
        rc.fileParams.clear();
        rc.request = null;
        rc.response = null;

        cache.remove();
    }

    private static Map<String, String> parseRequest(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>();

        Enumeration<String> names = request.getParameterNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            if(StringUtils.endsWith(name, "[]")) {
                params.put(name, StringUtils.join(Arrays.stream(request.getParameterValues(name)).map(StringUtils::trim).toArray(String[]::new),
                                                  ","));
            } else {
                params.put(name, StringUtils.trim(request.getParameter(name)));
            }
        }
        return params;
    }

    private static Map<String, String> parseMultipartRequest(HttpServletRequest request)
    throws ServletException {
        Map<String, String> params = new HashMap<>(16);

        log.debug("Init request data for upload file....");
        try {
            ServletFileUpload upload = new ServletFileUpload();
            FileItemIterator iter = upload.getItemIterator(request);

            while (iter.hasNext()) {
                FileItemStream item = iter.next();
                String name = item.getFieldName();
                InputStream stream = item.openStream();

                if (item.isFormField()) {
                    String value = Streams.asString(stream, DEFAULT_CHARSET);
                    params.put(name, value);
                } else {
                    String fileName = new File(item.getName()).getName();
                    if (StringUtils.isBlank(fileName)) {
                        continue;
                    }
                    log.debug("Save the upload file to temporary directory：{}", fileName);

//                    String outFileName = String.format("%d-%s.%s", System.currentTimeMillis(),
//                                                       RandomStringUtils.randomNumeric(12),
//                                                       StringUtils.substringAfterLast(fileName, "."));
                    File outFile = new File(PathUtils.buildTempPathWithDate(fileName));
                    PathUtils.makeDirectory(outFile.getParent());

                    BufferedInputStream bis = new BufferedInputStream(stream);
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outFile));
                    Streams.copy(bis, bos, true);

                    log.debug("Save the file path to a param[{}]：{}", name, outFile.getAbsolutePath());

                    params.put(name, outFile.getAbsolutePath());
                }
            }
        } catch (FileUploadException exp) {
            log.error("FILE UPLOAD EXCEPTION", exp);
            throw new ServletException("File upload net.osjava.qframe.exception: " + exp.getMessage());
        } catch (IOException exp) {
            log.error("FILE READ EXCEPTION", exp);
            throw new ServletException("File read net.osjava.qframe.exception: " + exp.getMessage());
        }

        return params;
    }

    public String get(String name) {
        return params.get(name);
    }

    public <T extends Exception> String get(String name, T exception) throws T {
        String value = params.get(name);
        if (StringUtils.isEmpty(value)) {
            log.debug("Get a parameter from request has an error: {}", exception.getMessage());
            throw exception;
        }

        return value;
    }

    public <T> T parseJsonTo(Class<T> clazz) {
        StringBuilder content = new StringBuilder(1024);
        try (BufferedReader reader = this.request.getReader()) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line);
            }
        } catch (IOException e) {
            log.error("Conference status callback has an exception: {}", e.getMessage());
        }
        return JsonUtils.fromJson(content.toString(), clazz);
    }

    public String get(String name, String defValue) {
        return StringUtils.defaultIfEmpty(params.get(name), defValue);
    }

    public int get(String name, int def) {
        return Integer.parseInt(get(name, String.valueOf(def)));
    }

    public long get(String name, long def) {
        return Long.parseLong(get(name, String.valueOf(def)));
    }

    public boolean get(String name, boolean def) {
        return Boolean.parseBoolean(get(name, String.valueOf(def)));
    }

    public <T extends Enum<T>> T get(String name, T def) {
        T[] values = def.getDeclaringClass()
                        .getEnumConstants();

        Optional<String> value = Optional.ofNullable(get(name));

        if (!value.isPresent()) return def;

        if (StringUtils.isNumeric(value.get())) {
            int index = Integer.parseInt(value.get());
            if (index < 0 || index >= values.length) return def;

            return values[index];
        } else {
            for (T item : values) {
                if (StringUtils.equalsAnyIgnoreCase(value.get(), item.name())) {
                    return item;
                }
            }
            return def;
        }
    }

    public void set(@NonNull String name, @NonNull String value) {
        params.put(name, value);
    }

    public String getHeader(@NonNull String key) {
        return request.getHeader(key);
    }

    public void setHeader(@NonNull String key, @NonNull String value) {
        response.setHeader(key, value);
    }

    public long getDateHeader(@NonNull String key) {
        return request.getDateHeader(key);
    }

    public void setDateHeader(@NonNull String key, long value) {
        response.setDateHeader(key, value);
    }

    public void setAttribute(@NonNull String key, Object value) {
        request.setAttribute(key, value);
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<T> getAttribute(String key) {
        return Optional.ofNullable((T) request.getAttribute(key));
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<T> getSession(String key) {
        return Optional.ofNullable((T) request.getSession(true)
                                              .getAttribute(key));
    }

    public void setSession(String key, Object value) {
        if(value == null) {
            request.getSession(true).removeAttribute(key);
        } else {
            request.getSession(true).setAttribute(key, value);
        }
    }

    public String getContextPath() {
        return this.context.getContextPath();
    }

    public String getRequestUri() {
        return this.request.getRequestURI();
    }

    public String getServletPath() {
        return this.request.getServletPath();
    }

    public String getAppUri() {
        return StringUtils.substringAfter(getRequestUri(), getServletPath());
    }

    @SneakyThrows(IOException.class)
    public void sendError(int code, String message) {
        if (StringUtils.isEmpty(message)) {
            response.sendError(code);
        } else {
            response.sendError(code, message);
        }
    }

    public void errorNotFound() {
        sendError(HttpServletResponse.SC_NOT_FOUND, null);
    }

    public void errorForbidden() {
        sendError(HttpServletResponse.SC_FORBIDDEN, null);
    }

    public void errorException(String message) {
        sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
    }

    public void closeCache() {
        setHeader("Pragma", "No-cache");
        setHeader("Cache-Control", "no-cache");
        setDateHeader("Expires", 0L);
    }

    @SneakyThrows
    public void forward(String uri) {
        request.getRequestDispatcher(uri)
               .forward(request, response);
    }

    @SneakyThrows
    public void redirect(String url) {
        response.sendRedirect(url);
    }

    public String getRemoteAddress() {
        String ip = getHeader("X-Forwarded-For");
        if (StringUtils.isNotBlank(ip)) {
            String[] ips = StringUtils.split(ip, ',');

            if (ips != null) {
                for (String tmpip : ips) {
                    if (StringUtils.isBlank(tmpip)) {
                        continue;
                    }

                    tmpip = tmpip.trim();
                    if (isIPAddr(tmpip)
                        && !tmpip.startsWith("10.")
                        && !tmpip.startsWith("192.168.")
                        && !"127.0.0.1".equals(tmpip)) {
                        return tmpip.trim();
                    }
                }
            }
        }

        ip = getHeader("x-real-ip");
        if (isIPAddr(ip)) {
            return ip;
        }
        ip = getRequest().getRemoteAddr();
        if (ip.indexOf('.') == -1) {
            ip = "127.0.0.1";
        }
        return ip;
    }

    public void forEachParams(BiConsumer<String, String> target) {
        this.params.forEach(target);
    }

    @Override public String toString() {
        return getClass().getSimpleName() + "(" + JsonUtils.toJson(params) + ")";
    }

    private boolean isIPAddr(String address) {
        if (StringUtils.isEmpty(address)) {
            return false;
        }
        String[] ips = StringUtils.split(address, '.');
        if (ips.length != 4) {
            return false;
        }
        try {
            int ipa = Integer.parseInt(ips[0]);
            int ipb = Integer.parseInt(ips[1]);
            int ipc = Integer.parseInt(ips[2]);
            int ipd = Integer.parseInt(ips[3]);
            return ipa >= 0 && ipa <= 255 && ipb >= 0 && ipb <= 255 && ipc >= 0 && ipc <= 255 && ipd >= 0 && ipd <= 255;
        } catch (Exception e) {
            // Ignore Exception, if throw net.osjava.qframe.exception, this method would be return FALSE.
        }
        return false;
    }
}
