package net.osjava.qframe.web.pebble;

import com.mitchellbosecke.pebble.error.PebbleException;
import com.mitchellbosecke.pebble.extension.Filter;
import com.mitchellbosecke.pebble.template.EvaluationContext;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import net.osjava.qframe.web.RequestContext;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class NumberMaskFilter implements Filter
{
    static final String FUNCTION_NAME = "numberMask";
    private static final String FIRST_COUNT = "first";
    private static final String LAST_COUNT = "last";
    private static final String MASK = "mask";

    @Override public Object apply(Object o, Map<String, Object> map, PebbleTemplate pebbleTemplate,
                                  EvaluationContext evaluationContext, int i)
    throws PebbleException {
        if(RequestContext.get().get("_debug_", false)) return o;

        AtomicInteger first = new AtomicInteger(3);
        Optional.ofNullable(map.get(FIRST_COUNT)).ifPresent(it -> first.set(((Long)it).intValue()));
        AtomicInteger last = new AtomicInteger(4);
        Optional.ofNullable(map.get(LAST_COUNT)).ifPresent(it -> last.set(((Long)it).intValue()));
        AtomicReference<String> mask = new AtomicReference<>("*");
        Optional.ofNullable(map.get(MASK)).ifPresent(it -> mask.set(it.toString()));

        String source = (String) o;
        String prefix = StringUtils.substring(source, 0, first.get());
        String suffix = StringUtils.substring(source, -1 * last.get());
        int maskLength = source.length() - first.get() - last.get();
        String maskString = StringUtils.repeat(mask.get(), maskLength);

        return prefix + maskString + suffix;
    }

    @Override public List<String> getArgumentNames() {
        return Arrays.asList(FIRST_COUNT, LAST_COUNT, MASK);
    }
}
