package net.osjava.qframe.web.pebble;

import com.mitchellbosecke.pebble.extension.Function;
import com.mitchellbosecke.pebble.template.EvaluationContext;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class StartWithFunction implements Function
{
    static final String FUNCTION_NAME = "startWith";
    private static final String ARG_SOURCE = "source";
    private static final String ARG_PREFIX = "prefix";

    @Override
    public Object execute(Map<String, Object> map, PebbleTemplate pebbleTemplate, EvaluationContext evaluationContext,
                          int i) {
        String source = (String) map.get(ARG_SOURCE);
        String prefix = (String) map.get(ARG_PREFIX);

        return StringUtils.startsWith(source, prefix);
    }

    @Override public List<String> getArgumentNames() {
        return Arrays.asList(ARG_SOURCE, ARG_PREFIX);
    }
}
