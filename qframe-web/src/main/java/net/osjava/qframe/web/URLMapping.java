package net.osjava.qframe.web;

import lombok.extern.slf4j.Slf4j;
import net.osjava.qframe.core.BeanFactory;
import net.osjava.qframe.utils.PathUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public final class URLMapping implements Filter
{
    private static String actionServlet = "/action/";
    private static String viewServlet = "/view/";
    private static List<String> ignoreList = Collections.emptyList();
    private static Set<String> cacheActionPath = Collections.emptySet();

    @Override public void init(FilterConfig filterConfig)
    throws ServletException {
        log.debug("Init URLMapping Filter...");

        actionServlet = StringUtils.defaultIfEmpty(filterConfig.getInitParameter("action"), actionServlet).trim();
        if(!StringUtils.endsWith(actionServlet, "/")) actionServlet += "/";

        viewServlet = StringUtils.defaultIfEmpty(filterConfig.getInitParameter("view"), viewServlet).trim();
        if(!StringUtils.endsWith(viewServlet, "/")) viewServlet += "/";

        String[] cfgIgnores = StringUtils.split(filterConfig.getInitParameter("ignore"), ",");
        ignoreList = Arrays.stream(cfgIgnores).map(String::trim).collect(Collectors.toCollection(ArrayList::new));

        cacheActionPath = BeanFactory.getBeansWithAnnotation(Controller.class)
                                     .keySet();
        cacheActionPath = cacheActionPath.stream()
                                         .sorted((o1, o2) -> Integer.compare(o2.length(), o1.length()))
                                         .collect(Collectors.toCollection(LinkedHashSet::new));

        if (log.isDebugEnabled()) {
            log.debug("Init Action mapping...");
            cacheActionPath.forEach(it -> log.debug("|- {}", it));
        }
        log.debug("Init URLMapping Filter...DONE.");
    }

    @Override public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String servletPath = StringUtils.substringAfter(httpRequest.getRequestURI(), httpRequest.getContextPath());
        if (checkIgnore(servletPath)) {
            chain.doFilter(request, response);
            return;
        }

        try {
            RequestContext rc = RequestContext.create(httpRequest, httpResponse, request.getServletContext());
            if (servletPath.startsWith(actionServlet)
                || servletPath.startsWith(viewServlet)) {
                log.debug("This URI [{}] will skips the URLMapping.", servletPath);
                chain.doFilter(request, response);
                return;
            }

            for (String path : cacheActionPath) {
                if (servletPath.startsWith(path)) {
                    log.info("The Request [{}}] will be forwarded to Action Servlet [{}].", servletPath, actionServlet);
                    rc.set(RequestContext.PARAM_ACTION_SERVLET, actionServlet);
                    rc.set(RequestContext.PARAM_VIEW_SERVLET, viewServlet);
                    rc.set(RequestContext.PARAM_ACTION_NAME, path);
                    rc.forward(PathUtils.concat(actionServlet, servletPath));
                    return;
                }
            }
            log.info("The Request [{}] will be forwarded to View Servlet.", servletPath);
            rc.forward(PathUtils.concat(viewServlet, servletPath));
        } finally {
            RequestContext.destroy();
        }
    }

    @Override public void destroy() {

    }

    private boolean checkIgnore(String url) {
        for (String item : ignoreList) {
            String uri = item.replace("*", "");

            if (item.startsWith("*")) {
                if (url.endsWith(uri)) return true;
            } else if (item.endsWith("*")) {
                if (url.startsWith(uri)) return true;
            } else {
                if (url.contains(uri)) return true;
            }
        }

        return false;
    }
}
