package net.osjava.qframe.web;

import lombok.extern.java.Log;
import net.osjava.qframe.core.AbstractApplication;
import net.osjava.qframe.utils.PathUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

@Log
public class ApplicationListener extends AbstractApplication implements ServletContextListener
{
    @Override public void contextInitialized(ServletContextEvent sce) {
        String contextPath = Optional.of(sce.getServletContext().getContextPath())
                                     .filter(it -> !it.equals("/"))
                                     .orElse("/root");

        String appId = Optional.ofNullable(sce.getServletContext().getInitParameter("application-id"))
                               .orElse(StringUtils.replace(contextPath, "/", "application-"));
        log.info(String.format("Init the application %s", appId));

        String homePath = Optional.ofNullable(sce.getServletContext().getInitParameter("workspace"))
                                  .orElse(System.getProperty(String.format("WORKSPACE_%s", appId)));

        if (StringUtils.isEmpty(homePath)) {
            String workspacePath = Optional.ofNullable(System.getProperty("WEBAPP_WORKSPACES"))
                                           .orElse(System.getenv("WEBAPP_WORKSPACES"));

            if (StringUtils.isNotEmpty(workspacePath)) {
                homePath = PathUtils.concat(workspacePath, contextPath);
            }
        }

        if (StringUtils.isEmpty(homePath)) {
            homePath = sce.getServletContext().getRealPath("/WEB-INF");
        }

        Properties appProperties = new Properties();
        try (InputStream input = new FileInputStream(sce.getServletContext().getRealPath("/META-INF/MANIFEST.MF"))) {
            appProperties.load(input);
            appProperties.forEach((key, value)-> sce.getServletContext().setAttribute(key.toString(), value.toString()));
        } catch (IOException e) {
            // ignore this exception
        }

        super.init(appId, homePath, appProperties);
        log.info(String.format("Init the application %s with the workspace %s successfully.", appId, homePath));
    }

    @Override public void contextDestroyed(ServletContextEvent sce) {
        super.destroy();
    }
}
