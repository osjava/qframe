package net.osjava.qframe.web;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper = false)
public class ActionException extends RuntimeException
{
    @Getter private final long code;

    protected ActionException(long code, String message) {
        super(message);
        this.code = code;
    }

    public static ActionException with(long code, String message) {
        return new ActionException(code, message);
    }
}
