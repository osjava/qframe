package net.osjava.qframe.web.pebble;

import com.mitchellbosecke.pebble.extension.Function;
import com.mitchellbosecke.pebble.template.EvaluationContext;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import net.osjava.qframe.web.RequestContext;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class EnvFunction implements Function
{
    static final String FUNCTION_NAME = "env";

    private static final String ARG_KEY = "key";

    @Override
    public Object execute(Map<String, Object> map, PebbleTemplate pebbleTemplate, EvaluationContext evaluationContext,
                          int i) {
        String key = (String) map.get(ARG_KEY);
        RequestContext rc = RequestContext.get();
        String value = (String) rc.getAttribute(key).orElse(rc.getContext().getAttribute(key));
        if (StringUtils.isEmpty(value)) {
            value = System.getProperty(key);
        }

        return StringUtils.defaultIfEmpty(value, System.getenv(key));
    }

    @Override public List<String> getArgumentNames() {
        return Collections.singletonList(ARG_KEY);
    }
}
