package net.osjava.qframe.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.format.DateTimeFormatter.ofPattern;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class JsonUtils
{
    public static JsonElement toJsonTree(@NotNull Object object) {
        return getBuilder().create()
                           .toJsonTree(object);
    }

    public static String toJson(@NotNull Object object) {
        return getBuilder().create()
                           .toJson(object);
    }

    public static <T> T fromJson(@NotNull String json, @NotNull Class<T> clazz) {
        try {
            return getBuilder().create()
                               .fromJson(json, clazz);
        } catch (Exception e) {
            log.error("Convert string to json error: {} : {}", json, e.getMessage());
            return null;
        }
    }

    public static String format(@NotNull JsonElement jsonElement) {
        return format(jsonElement, StringUtils.EMPTY);
    }

    public static String format(@NotNull JsonElement jsonElement, String description) {
        StringBuilder builder = new StringBuilder(1024);
        if (StringUtils.isNotEmpty(description)) {
            builder.append("=== BEGIN < ")
                   .append(description)
                   .append(" > ===\n");
        }

        builder.append(getBuilder().setPrettyPrinting()
                                   .create()
                                   .toJson(jsonElement));

        if (StringUtils.isNotEmpty(description)) {
            builder.append("\n=== END < ")
                   .append(description)
                   .append(" > ===");
        }

        return builder.toString();
    }

    private static GsonBuilder getBuilder() {
        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(LocalDate.class,
                                    (JsonSerializer<LocalDate>) (src, typeOfSrc, context)
                                        -> new JsonPrimitive(src.format(DateTimeFormatter.ISO_DATE)));

        builder.registerTypeAdapter(LocalDate.class,
                                    (JsonDeserializer<LocalDate>) (json, typeOfT, context)
                                        -> LocalDate.parse(json.getAsString()));

        builder.registerTypeAdapter(LocalDateTime.class,
                                    (JsonSerializer<LocalDateTime>) (src, typeOfSrc, context)
                                        -> new JsonPrimitive(src.format(ofPattern("yyyy-MM-dd HH:mm:ss"))));

        builder.registerTypeAdapter(LocalDateTime.class,
                                    (JsonDeserializer<LocalDateTime>) (json, typeOfT, context)
                                        -> LocalDateTime.parse(json.getAsString(), ofPattern("yyyy-MM-dd HH:mm:ss")));
        return builder;
    }
}
