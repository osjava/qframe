package net.osjava.qframe.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class HttpUtils {
    public static final String LOCALHOST = "127.0.0.1";

    private static final String UTF8_ENCODE = "UTF-8";
    private static final int DEFAULT_TIMEOUT = 5000;

    public static void init() {
    }

    public static void destroy() {
    }

    public static Response<JsonElement> postStream(String url, JsonElement content, Header... headers) {
        return postStream(url, content, DEFAULT_TIMEOUT, headers);
    }

    public static Response<JsonElement> postStream(String url, JsonElement content, int timeout, Header... headers) {
        ArrayUtils.add(headers, Header.with(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.getMimeType()));
        Response<String> response = postStream(url, content.toString(), ContentType.APPLICATION_JSON, timeout, headers);

        if (response.isSuccess()) {
            JsonElement jsonElement = JsonUtils.fromJson(response.content, JsonElement.class);
            if (jsonElement == null) jsonElement = JsonNull.INSTANCE;
            return Response.with(response.statusCode, jsonElement);
        } else {
            return Response.with(response.statusCode, new JsonPrimitive(response.content));
        }
    }

    public static Response<String> postStream(String url, String content, Header... headers) {
        return postStream(url, content, ContentType.APPLICATION_XHTML_XML, DEFAULT_TIMEOUT, headers);
    }

    public static Response<String> postStream(String url, String content, int timeout, Header... headers) {
        return postStream(url, content, ContentType.APPLICATION_XHTML_XML, timeout, headers);
    }

    public static Response<String> postStream(@NonNull String url, @NonNull String content,
                                              @NonNull ContentType contentType, int timeout, Header... headers) {

        log.info(">>> Request remote url by post stream: url={}, content={}, contentType={}, timeout={}, headers={}",
                 url, content, contentType, timeout, headers);

        StringEntity entity = new StringEntity(content, contentType.withCharset(UTF8_ENCODE));

        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(getRequestConfig(timeout));
        httpPost.setEntity(entity);

        for (Header item : headers) {
            httpPost.setHeader(item.name, item.value);
        }

        Response<String> result = doMethod(httpPost);
        log.info("<<< Request from url<{}>: {}", url, result);
        return result;
    }

    public static Response<String> postForm(@NonNull String url, @NonNull List<Parameter> parameters,
                                            Header... headers) {
        return postForm(url, parameters, DEFAULT_TIMEOUT, headers);
    }

    public static Response<String> postForm(@NonNull String url, Parameter... parameters) {
        return postForm(url, Arrays.asList(parameters), DEFAULT_TIMEOUT);
    }

    public static Response<String> postForm(@NonNull String url, int timeout, Parameter... parameters) {
        return postForm(url, Arrays.asList(parameters), timeout);
    }

    @SneakyThrows(UnsupportedEncodingException.class)
    public static Response<String> postForm(@NonNull String url, @NonNull List<Parameter> parameters, int timeout,
                                            Header... headers) {
        log.info(">>> Request remote url by form: url={}, parameters={}, timeout={}, headers={}", url, parameters,
                 timeout, headers);

        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(getRequestConfig(timeout));

        List<NameValuePair> params = new ArrayList<>(parameters.size());
        for (Parameter item : parameters) {
            params.add(new BasicNameValuePair(item.name, item.value));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(params, UTF8_ENCODE));

        for (Header item : headers) {
            httpPost.setHeader(item.name, item.value);
        }

        Response<String> result = doMethod(httpPost);
        log.info("<<< Request from url<{}>: {}", url, result);
        return result;
    }

    public static Response<String> request(@NonNull String url, Header... headers) {
        return request(url, Collections.emptyList(), DEFAULT_TIMEOUT, headers);
    }

    public static Response<String> request(@NonNull String url, int timeout, Header... headers) {
        return request(url, Collections.emptyList(), timeout, headers);
    }

    public static Response<String> request(@NonNull String url, int timeout, Parameter... parameters) {
        return request(url, Arrays.asList(parameters), timeout);
    }

    @SneakyThrows
    public static Response<String> request(@NonNull String url, @NonNull List<Parameter> parameters, int timeout,
                                           Header... headers) {
        log.info(">>> Request remote url: url={}, parameters={}, timeout={}, headers={}", url, parameters, timeout,
                 headers);

        URIBuilder uriBuilder = new URIBuilder(url);
        for (Parameter item : parameters) {
            uriBuilder.addParameter(item.name, item.value);
        }

        HttpGet httpGet = new HttpGet(uriBuilder.build());
        httpGet.setConfig(getRequestConfig(timeout));

        Response<String> result = doMethod(httpGet);

        log.info("<<< Response from url<{}>: {}", url, result);
        return result;
    }

    private static RequestConfig getRequestConfig(int timeout) {
        return RequestConfig.custom()
                            .setSocketTimeout(timeout)
                            .setConnectTimeout(timeout)
                            .setConnectionRequestTimeout(timeout)
                            .build();
    }

    private static Response<String> doMethod(HttpRequestBase httpMethod) {
        httpMethod.setHeader("Connection", "close");

        CloseableHttpClient httpClient = HttpClients.custom()
                                                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                                                    .disableAutomaticRetries()
                                                    .build();

        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpMethod);
            StatusLine statusLine = response.getStatusLine();
            if (log.isDebugEnabled()) {
                log.debug("Response from url <{}>: {}", httpMethod.getURI(), response.getStatusLine());
                Arrays.stream(response.getAllHeaders())
                      .forEach(it -> log.debug("{} = {}", it.getName(), it.getValue()));
            }

            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                return Response.with(statusLine.getStatusCode(),
                                     EntityUtils.toString(response.getEntity(), "utf-8"));
            } else {
                httpMethod.abort();
            }

            return Response.with(statusLine.getStatusCode(), statusLine.getReasonPhrase());
        } catch (IOException e) {
            return Response.with(HttpStatus.SC_BAD_GATEWAY, e.getMessage());
        } finally {
            httpMethod.releaseConnection();
            try {
                if (response != null) {
                    EntityUtils.consumeQuietly(response.getEntity());
                    response.close();
                }

                httpClient.close();
            } catch (IOException e) {
                log.error("Close HttpClient has an error: {}", e.getMessage());
            }
        }
    }

    @RequiredArgsConstructor(staticName = "with")
    @ToString
    public static class Header {
        @Getter @NotNull private String name;
        @Getter @NotNull private String value;
    }

    @RequiredArgsConstructor(staticName = "with")
    @ToString
    public static class Parameter {
        @Getter @NotNull private String name;
        @Getter @NotNull private String value;

        public static List<Parameter> valueOf(@NonNull Map<String, String> values) {
            List<Parameter> parameters = new ArrayList<>(values.size());
            values.forEach((key, value) -> parameters.add(Parameter.with(key, value)));
            return Collections.unmodifiableList(parameters);
        }
    }

    @RequiredArgsConstructor(staticName = "with")
    @ToString
    public static class Response<T> {
        @Getter @NotNull private Integer statusCode;
        @Getter @NotNull private T content;

        public boolean isSuccess() {
            return statusCode == HttpStatus.SC_OK;
        }
    }
}
