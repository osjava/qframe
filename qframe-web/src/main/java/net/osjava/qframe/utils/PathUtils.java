package net.osjava.qframe.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import net.osjava.qframe.core.Configuration;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang3.StringUtils.endsWith;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PathUtils
{
    public static String concat(@NotNull String basePath, String... paths) {
        StringBuilder builder = new StringBuilder(1024);
        builder.append(basePath.endsWith("/") ? basePath.substring(0, basePath.length() - 1) : basePath);

        for (String path : paths) {
            if (!StringUtils.startsWith(path, "/")) {
                builder.append("/");
            }
            builder.append(endsWith(path, "/") ? substringBeforeLast(path, "/") : path);
        }

        return StringUtils.defaultIfEmpty(builder.toString(), basePath);
    }

    public static String buildTempPath(String... paths) {
        return concat(Configuration.getWorkspace(), ArrayUtils.insert(0, paths, "/temp"));
    }

    public static String buildTempPathWithDate(String... paths) {
        return buildTempPath(ArrayUtils.insert(0, paths, LocalDate.now()
                                                                  .format(DateTimeFormatter.ISO_DATE)));
    }

    public static boolean makeDirectory(String path) {
        File fPath = new File(path);
        if (fPath.exists() && fPath.isDirectory()) return true;
        if (fPath.exists() && fPath.isFile()) return false;

        return fPath.mkdirs();
    }

    public static boolean isFile(String path) {
        File fPath = new File(path);
        return fPath.exists() && fPath.isFile();
    }

    public static boolean isDirectory(String path) {
        File fPath = new File(path);
        return fPath.exists() && fPath.isDirectory();
    }

    public static void move(String source, String target) {
        File fPath = new File(source);
        File fTarget = new File(target);
        if (!isDirectory(fTarget.getParent())) {
            makeDirectory(fTarget.getParent());
        }

        fPath.renameTo(fTarget);
    }

    public static boolean remove(String path) {
        File fPath = new File(path);

        if (isDirectory(path)) {
            File[] children = fPath.listFiles();
            if (children != null) {
                for (File file : children) {
                    remove(file.getAbsolutePath());
                }
            }
        }

        return fPath.delete();
    }

    public static List<String> list(String path) {
        if (isFile(path)) return Collections.singletonList(StringUtils.substringAfterLast(path, "/"));

        File fPath = new File(path);
        String[] fileList = fPath.list();
        if (fileList == null) return new ArrayList<>(0);

        return Arrays.asList(fileList);
    }

    public static void zip(String zipFile, String... files)
    throws IOException {
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile))) {
            for (String file : files) {
                File source = new File(file);
                appendToZip(source, source.getName(), zos);
            }
        }
    }

    public static void unzip(String source, String target)
    throws IOException {
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(source))) {
            boolean isDirector = PathUtils.isDirectory(target);
            ZipEntry entry;
            File outputFile;
            while ((entry = zis.getNextEntry()) != null) {
                if (isDirector) {
                    outputFile = new File(target, entry.getName());
                    if (entry.isDirectory()) {
                        PathUtils.makeDirectory(outputFile.getAbsolutePath());
                        continue;
                    }
                } else {
                    outputFile = new File(target);
                }

                byte[] buffer = new byte[1024];
                int length;
                try (FileOutputStream fos = new FileOutputStream(outputFile.getCanonicalFile())) {
                    while ((length = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, length);
                    }
                }
            }
            zis.closeEntry();
        }
    }

    private static void appendToZip(File source, String entityName, ZipOutputStream output)
    throws IOException {
        if (source.isHidden()) return;

        if (source.isDirectory()) {
            if (StringUtils.endsWith(entityName, "/")) {
                output.putNextEntry(new ZipEntry(entityName));
            } else {
                output.putNextEntry(new ZipEntry(entityName + "/"));
            }
            output.closeEntry();

            File[] children = source.listFiles();
            if (children == null) return;

            for (File child : children) {
                appendToZip(child, entityName + "/" + child.getName(), output);
            }
            return;
        }

        try (FileInputStream input = new FileInputStream(source)) {
            ZipEntry zipEntry = new ZipEntry(entityName);
            output.putNextEntry(zipEntry);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        }
    }
}
