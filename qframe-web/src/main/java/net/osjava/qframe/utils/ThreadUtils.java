package net.osjava.qframe.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ThreadUtils
{
    @SneakyThrows public static void sleepSecond(long timeout) {
        TimeUnit.SECONDS.sleep(timeout);
    }

    @SneakyThrows public static void sleepMilliseconds(long timeout) {
        TimeUnit.MILLISECONDS.sleep(timeout);
    }

    @SneakyThrows public static void sleep(TimeUnit timeUnit, long timeout) {
        timeUnit.sleep(timeout);
    }
}
