package net.osjava.qframe.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class ClassUtils
{
    public static Optional<Method> getMethod(@NotNull Class<?> clazz, @NotNull String name, Class<?>... parameters) {
        log.debug("Lookup the method [{}] with parameters [{}] in the class [{}]",
                  name, parameters, clazz.getName());
        try {
            return Optional.of(clazz.getMethod(name, parameters));
        } catch (NoSuchMethodException e) {
            return Optional.empty();
        }
    }
}
