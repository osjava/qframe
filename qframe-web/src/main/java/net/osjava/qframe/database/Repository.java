package net.osjava.qframe.database;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

@Slf4j
@org.springframework.stereotype.Repository
public class Repository
{
    private final SessionFactory sessionFactory;

    public Repository(@NotNull SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Optional<T> get(@NotNull Class<T> clazz, @NotNull Serializable uid) {
        return Optional.ofNullable(getSession().get(clazz, uid));
    }

    @SuppressWarnings({"unchecked"})
    @Transactional(rollbackFor = {Exception.class})
    public <PK extends Serializable> PK save(@NotNull IEntity entity) {
        return (PK) getSession().save(entity);
    }

    @SuppressWarnings("unchecked")
    @Transactional(rollbackFor = {Exception.class})
    public <PK extends Serializable> PK merge(@NotNull IEntity entity) {
        if (entity.getUid() instanceof Number) {
            if(entity.getUid() == null) {
                return save(entity);
            }

            Number uid = (Number) entity.getUid();
            if (uid.longValue() > 0) {
                update(entity);
                return (PK) entity.getUid();
            } else {
                return save(entity);
            }
        } else {
            if (entity.getUid() == null) {
                return save(entity);
            } else {
                update(entity);
                return (PK) entity.getUid();
            }
        }
    }

    @Transactional(rollbackFor = {Exception.class})
    public void update(@NotNull IEntity entity) {
        getSession().update(entity);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void delete(@NotNull IEntity entity) {
        getSession().delete(entity);
    }

    @Transactional(rollbackFor = Exception.class)
    public <PK extends Serializable, T extends IEntity> void delete(@NotNull Class<T> clazz, @NotNull PK uid) {
        get(clazz, uid).ifPresent(it -> getSession().delete(it));

    }

    @Transactional(readOnly = true)
    public <T> List<T> find(Function<CriteriaBuilder, CriteriaQuery<T>> buildQuery) {
        return find(Pagination.DEFAULT_SIZE, buildQuery);
    }

    @Transactional(readOnly = true)
    public <T> List<T> find(int maxResult, @NotNull Function<CriteriaBuilder, CriteriaQuery<T>> buildQuery) {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();

        CriteriaQuery<T> query = buildQuery.apply(builder);

        return getSession().createQuery(query)
                           .setMaxResults(maxResult)
                           .getResultList();
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz) {
        return find(clazz, 0, Pagination.DEFAULT_SIZE, null, null);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz, int maxResult) {
        return find(clazz, 0, maxResult, null, null);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz, int maxResult,
                                            List<net.osjava.qframe.database.Order> orders) {
        return find(clazz, 0, maxResult, orders, null);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz, List<net.osjava.qframe.database.Order> orders) {
        return find(clazz, 0, Pagination.DEFAULT_SIZE, orders, null);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz, BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        return find(clazz, 0, Pagination.DEFAULT_SIZE, null, filter);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz, List<net.osjava.qframe.database.Order> orders,
                                            BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        return find(clazz, 0, Pagination.DEFAULT_SIZE, orders, filter);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz, int maxResult,
                                            BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        return find(clazz, 0, maxResult, null, filter);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz, int maxResult,
                                            List<net.osjava.qframe.database.Order> orders,
                                            BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        return find(clazz, 0, maxResult, orders, filter);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> find(@NotNull Class<T> clazz,
                                            int firstIndex,
                                            int maxResult,
                                            List<net.osjava.qframe.database.Order> orders,
                                            BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        log.debug("Find {} by: firstIndex={}, maxResult={}, orders={}, filter={}",
                  clazz, firstIndex, maxResult, orders, filter);

        CriteriaBuilder builder = getSession().getCriteriaBuilder();

        CriteriaQuery<T> query = builder.createQuery(clazz);
        Root<T> root = query.from(clazz);
        query.select(root);

        if (filter != null) {
            Optional.ofNullable(filter.apply(builder, root)).ifPresent(query::where);
        }

        Optional.ofNullable(orders).ifPresent(it->{
            Order[] expressions = new Order[it.size()];
            for (int i = 0, n = it.size(); i < n; i++) {
                net.osjava.qframe.database.Order item = it.get(i);
                if (item.isAsc()) {
                    expressions[i] = builder.asc(root.get(item.getName()));
                } else {
                    expressions[i] = builder.desc(root.get(item.getName()));
                }
            }

            query.orderBy(expressions);
        });

        List<T> result = getSession().createQuery(query).setFirstResult(firstIndex).setMaxResults(maxResult)
                                     .getResultList();
        if (log.isDebugEnabled()) {
            log.debug("Result for querying: {}", result);
        }
        return result;
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> findBy(@NotNull Class<T> clazz, @NotNull String property, Object value) {
        return find(clazz, ((builder, root) -> {
            if (value == null) return builder.isNull(root.get(property));

            return builder.equal(root.get(property), value);
        }));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> findBy(@NotNull Class<T> clazz, @NotNull String property, Object value, int maxResult) {
        log.debug("Find {} by: property={}, value={}, maxResult={}", clazz, property, value, maxResult);
        return find(clazz, maxResult, ((builder, root) -> {
            if (value == null) return builder.isNull(root.get(property));

            return builder.equal(root.get(property), value);
        }));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> findBy(@NotNull Class<T> clazz, @NotNull String property, Object value,
                                              List<net.osjava.qframe.database.Order> orders) {
        return find(clazz, orders, ((builder, root) -> {
            if(value == null) return builder.isNull(root.get(property));

            return builder.equal(root.get(property), value);
        }));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> List<T> findBy(@NotNull Class<T> clazz, @NotNull String property, Object value,
                                              List<net.osjava.qframe.database.Order> orders,
                                              int maxResult) {
        return find(clazz, maxResult, orders, ((builder, root) -> {
            if(value == null) return builder.isNull(root.get(property));

            return builder.equal(root.get(property), value);
        }));
    }

    @Transactional(readOnly = true)
    public <T> Optional<T> findFirst(@NotNull Function<CriteriaBuilder, CriteriaQuery<T>> buildQuery) {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();

        CriteriaQuery<T> query = buildQuery.apply(builder);

        List<T> result = getSession().createQuery(query)
                                     .setMaxResults(1)
                                     .getResultList();
        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.ofNullable(result.get(0));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Optional<T> findFirst(@NotNull Class<T> clazz,
                                                     BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        List<T> result = find(clazz, 1, filter);

        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result.get(0));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Optional<T> findFirst(@NotNull Class<T> clazz,
                                                     List<net.osjava.qframe.database.Order> orders,
                                                     BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        List<T> result = find(clazz, 1, orders, filter);

        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result.get(0));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Optional<T> findFirstBy(@NotNull Class<T> clazz, @NotNull String property, Object value) {
        List<T> result = findBy(clazz, property, value, 1);

        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result.get(0));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Optional<T> findFirstBy(@NotNull Class<T> clazz, @NotNull String property, Object value,
                                                       List<net.osjava.qframe.database.Order> orders) {
        List<T> result = findBy(clazz, property, value, orders, 1);
        return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.of(result.get(0));
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Pagination<T> findPage(@NotNull Class<T> clazz, int pageNo, int pageSize,
                                                      BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {

        return findPage(clazz, pageNo, pageSize, null, filter);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Pagination<T> findPage(@NotNull Class<T> clazz, int pageNo, int pageSize,
                                                      List<net.osjava.qframe.database.Order> orders) {

        return findPage(clazz, pageNo, pageSize, orders, null);
    }

    @Transactional(readOnly = true)
    public <T extends IEntity> Pagination<T> findPage(@NotNull Class<T> clazz, int pageNo, int pageSize,
                                                      List<net.osjava.qframe.database.Order> orders,
                                                      BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        long totalCount = this.count(clazz, filter);
        int firstIndex = (pageNo - 1) * pageSize;

        List<T> result = find(clazz, firstIndex, pageSize, orders, filter);

        return Pagination.with(pageNo, pageSize, totalCount, result);
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private <T extends IEntity> long count(@NotNull Class<T> clazz,
                                           BiFunction<CriteriaBuilder, Root<T>, Predicate> filter) {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();

        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> root = query.from(clazz);
        query.select(builder.count(root));

        if (Optional.ofNullable(filter).isPresent()) {
            Optional<Predicate> conditions = Optional.ofNullable(filter.apply(builder, root));
            conditions.ifPresent(query::where);
        }

        return getSession().createQuery(query)
                           .getSingleResult();
    }
}
