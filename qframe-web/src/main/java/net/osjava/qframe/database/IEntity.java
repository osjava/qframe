package net.osjava.qframe.database;

import com.google.gson.JsonElement;
import net.osjava.qframe.utils.JsonUtils;

import java.io.Serializable;

public interface IEntity
{
    Serializable getUid();
    default JsonElement buildJson() {
        return JsonUtils.toJsonTree(this);
    }
}
