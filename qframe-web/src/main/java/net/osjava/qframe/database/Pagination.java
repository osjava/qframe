package net.osjava.qframe.database;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.osjava.qframe.utils.JsonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Pagination<T>
{
    public static final int FIRST = 1;
    public static final int DEFAULT_SIZE = 200;

    @Getter private int first = FIRST;
    @Getter private int last;

    @Getter private int pageNo;
    @Getter private int pageSize;
    @Getter private long totalCount;
    @Getter private List<T> data = new ArrayList<>(0);

    public static <V> Pagination<V> with(int pageNo, int pageSize, long total, List<V> data) {
        Pagination<V> pagination = new Pagination<>();
        pagination.pageNo = pageNo;
        pagination.pageSize = pageSize;
        pagination.totalCount = total;
        pagination.first = total > 0 ? FIRST : 0;
        pagination.last = pageSize == 0 ? 0 : (int) Math.ceil(total * 1.0 / pageSize);

        if(data != null) {
            pagination.data = data;
        }

        return pagination;
    }

    public static <V> Pagination<V> empty() {
        return Pagination.with(0, 0, 0, Collections.emptyList());
    }

    public final int next() {
        return pageNo < last ? (pageNo + 1) : last;
    }

    public final int previous() {
        return pageNo > first ? (pageNo - 1) : first;
    }

    public boolean hasNext() {
        return pageNo < last;
    }

    public boolean hasPrevious() {
        return pageNo > first;
    }

    public JsonObject buildJson() {
        JsonObject result = new JsonObject();
        result.addProperty("pageNo", pageNo);
        result.addProperty("pageSize", pageSize);
        result.addProperty("firstPage", first);
        result.addProperty("lastPage", last);
        result.addProperty("total", totalCount);

        JsonArray rows = new JsonArray();
        data.forEach(it->rows.add(JsonUtils.toJsonTree(it)));

        result.add("rows", rows);
        return result;
    }
}
