package net.osjava.qframe.database;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Order
{
    @Getter @NotNull private final String name;
    @Getter private final boolean asc;

    public static Order asc(@NotNull String name) {
        return new Order(name, true);
    }

    public static Order desc(@NotNull String name) {
        return new Order(name, false);
    }
}
