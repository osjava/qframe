package net.osjava.qframe.datatype;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CoreEvents
{
    public static class InitApplication extends Event<String>
    {
        public InitApplication(@NotNull Object source, @NotNull String data) {
            super(source, data);
        }
    }

    public static class DestroyApplication extends Event<String>
    {
        public DestroyApplication(@NotNull Object source, @NotNull String data) {
            super(source, data);
        }
    }
}
