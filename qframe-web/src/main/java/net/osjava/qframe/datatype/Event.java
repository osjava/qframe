package net.osjava.qframe.datatype;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.EventObject;

public abstract class Event<T> extends EventObject
{
    @Getter private final T data;

    public Event(@NotNull Object source, T data) {
        super(source);
        if (data == null) throw new NullPointerException();

        this.data = data;
    }
}
