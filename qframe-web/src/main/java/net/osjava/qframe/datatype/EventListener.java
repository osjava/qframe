package net.osjava.qframe.datatype;

import net.osjava.qframe.exception.EventListenerException;
import org.jetbrains.annotations.NotNull;

@FunctionalInterface
public interface EventListener<T>
{
    void onEvent(@NotNull Event<T> event)
    throws EventListenerException;
}
