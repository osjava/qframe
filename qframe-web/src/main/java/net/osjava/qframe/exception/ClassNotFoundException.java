package net.osjava.qframe.exception;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.function.Supplier;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ClassNotFoundException extends RuntimeException
{
    private ClassNotFoundException(String s) {
        super(s);
    }

    public static Supplier<ClassNotFoundException> bind(String s) {
        return () -> new ClassNotFoundException(s);
    }
}
