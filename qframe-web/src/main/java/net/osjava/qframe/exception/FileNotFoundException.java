package net.osjava.qframe.exception;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.function.Supplier;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class FileNotFoundException extends RuntimeException
{
    private FileNotFoundException(String s) {
        super(s);
    }

    public static Supplier<FileNotFoundException> bind(String message) {
        return () -> new FileNotFoundException(message);
    }
}
