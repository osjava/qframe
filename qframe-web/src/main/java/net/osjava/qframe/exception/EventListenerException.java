package net.osjava.qframe.exception;

import java.util.function.Supplier;

public class EventListenerException extends RuntimeException
{
    private EventListenerException(String message) {
        super(message);
    }

    public static Supplier<EventListenerException> bind(String message) {
        return () -> new EventListenerException(message);
    }
}
